package com.edutilos.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * Created by Nijat Aghayev on 14.02.19.
 */
public class ControllerSearchSprint {
    @FXML
    private DatePicker dpDateFrom, dpDateTill;
    @FXML
    private TextField tfSprintName, tfUserStoriesMin, tfUserStoriesMax;
    @FXML
    private Button btnSearch;
    @FXML
    private AnchorPane paneSearchResults;
}
