package com.edutilos.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * Created by Nijat Aghayev on 14.02.19.
 */
public class ControllerWindowOverview {
    @FXML
    private Button btnOpenLatestSprint, btnSearchSprint, btnCreateNewSprint, btnManageTeam;

    @FXML
    private void initialize() {
    }


    @FXML
    private void handleBtnOpenLatestSprint() {

    }

    @FXML
    private void handleBtnSearchSprint() {

    }

    @FXML
    private void handleBtnCreateNewSprint() {

    }

    @FXML
    private void handleBtnManageTeam() {

    }
}
