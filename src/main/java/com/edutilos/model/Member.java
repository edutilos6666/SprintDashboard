package com.edutilos.model;

import javafx.scene.image.Image;
import lombok.Data;

/**
 * Created by  Nijat Aghayev on 14.02.19.
 */
@Data
public class Member {
    private String fname;
    private String lname;
    private Gender gender;
    private String ddgPosition;
    private SprintPosition sprintPosition;
    private Image image;
    private String email;
    private String phoneNumber;
}
