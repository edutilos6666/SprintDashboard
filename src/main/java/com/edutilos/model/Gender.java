package com.edutilos.model;

/**
 * Created by  Nijat Aghayev on 14.02.19.
 */
public enum Gender {
    Male, Female;
}
